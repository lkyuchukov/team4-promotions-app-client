import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ActivityActionPipe } from '../common/pipes/activity-action.pipe';
import { EntityTypePipe } from '../common/pipes/entity-type.pipe';
import { IconPipe } from '../common/pipes/icon.pipe';
import { SharedModule } from '../shared/shared.module';
import { ActivityComponent } from './activity/activity.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DetailedRedemptionsChartComponent } from './detailed-redemptions-chart/detailed-redemptions-chart.component';
import { NumbersCardComponent } from './numbers-card/numbers-card.component';
import { PrizeChartComponent } from './prize-chart/prize-chart.component';
import { PrizeTodayChartComponent } from './prize-today-chart/prize-today-chart.component';
import { RedemptionsChartComponent } from './redemptions-chart/redemptions-chart.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ActivityComponent,
    EntityTypePipe,
    ActivityActionPipe,
    IconPipe,
    PrizeChartComponent,
    RedemptionsChartComponent,
    NumbersCardComponent,
    DetailedRedemptionsChartComponent,
    PrizeTodayChartComponent,
  ],
  imports: [SharedModule, DashboardRoutingModule, NgxChartsModule],
})
export class DashboardModule {}
