import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-prize-today-chart',
  templateUrl: './prize-today-chart.component.html',
  styleUrls: ['./prize-today-chart.component.css'],
})
export class PrizeTodayChartComponent implements OnInit, OnDestroy {
  @Input()
  detailedPrizeInfo: any;

  dataSource = [];
  colorScheme: any;
  currentTheme: string;
  label = 'Prizes redeemed today';

  private themeSub: Subscription;

  constructor(private themeService: ThemeService) {}

  ngOnInit() {
    this.themeSub = this.themeService.theme$.subscribe(value => {
      if (value === 'blue') {
        this.colorScheme = {
          domain: ['#FFCDD2', '#E57373', '#C62828'],
        };
      } else if (value === 'red') {
        this.colorScheme = {
          domain: ['#C5CAE9', '#3F51B5', '#1A237E'],
        };
      } else {
        this.colorScheme = {
          domain: ['#CFD8DC', '#607D8B', '#37474F'],
        };
      }
    });

    this.dataSource = [
      {
        name: 'Small prizes',
        value: this.detailedPrizeInfo['Small prize'] || 0,
      },
      {
        name: 'Medium prizes',
        value: this.detailedPrizeInfo['Medium prize'] || 0,
      },
      {
        name: 'Big prizes',
        value: this.detailedPrizeInfo['Big prize'] || 0,
      },
    ];
  }

  ngOnDestroy() {
    this.themeSub.unsubscribe();
  }
}
