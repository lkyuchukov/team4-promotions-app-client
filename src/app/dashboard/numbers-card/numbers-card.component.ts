import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-numbers-card',
  templateUrl: './numbers-card.component.html',
  styleUrls: ['./numbers-card.component.css'],
})
export class NumbersCardComponent implements OnInit, OnDestroy {
  @Input()
  entityCount: any;

  // chart options
  dataSource = [];
  colorScheme: any;
  currentTheme: string;

  private themeSub: Subscription;

  constructor(private themeService: ThemeService) {}

  ngOnInit() {
    this.themeSub = this.themeService.theme$.subscribe(value => {
      if (value === 'blue') {
        this.colorScheme = {
          domain: ['#FFCDD2', '#E57373', '#C62828'],
        };
      } else if (value === 'red') {
        this.colorScheme = {
          domain: ['#C5CAE9', '#3F51B5', '#1A237E'],
        };
      } else {
        this.colorScheme = {
          domain: ['#CFD8DC', '#607D8B', '#37474F'],
        };
      }
    });

    this.dataSource = [
      {
        name: 'Customers',
        value: this.entityCount.customerCount,
      },
      {
        name: 'Outlets',
        value: this.entityCount.outletCount,
      },
      {
        name: 'Users',
        value: this.entityCount.userCount,
      },
    ];
  }

  ngOnDestroy() {
    this.themeSub.unsubscribe();
  }
}
