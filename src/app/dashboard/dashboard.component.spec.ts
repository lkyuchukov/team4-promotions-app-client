import { ScrollingModule } from '@angular/cdk/scrolling';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { of } from 'rxjs';
import { ActivityActionPipe } from '../common/pipes/activity-action.pipe';
import { EntityTypePipe } from '../common/pipes/entity-type.pipe';
import { IconPipe } from '../common/pipes/icon.pipe';
import { SharedModule } from '../shared/shared.module';
import { ActivityComponent } from './activity/activity.component';
import { DashboardComponent } from './dashboard.component';
import { NumbersCardComponent } from './numbers-card/numbers-card.component';
import { PrizeChartComponent } from './prize-chart/prize-chart.component';
import { RedemptionsChartComponent } from './redemptions-chart/redemptions-chart.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        NgxChartsModule,
        ScrollingModule,
      ],
      declarations: [
        DashboardComponent,
        ActivityComponent,
        IconPipe,
        ActivityActionPipe,
        EntityTypePipe,
        PrizeChartComponent,
        RedemptionsChartComponent,
        NumbersCardComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              dashboardInfo: {
                activity: [{}],
                prizeInfo: {
                  smallPrizesCount: 50,
                  mediumPrizesCount: 50,
                  bigPrizesCount: 19,
                },
                redemptionsToday: [],
                redemptionsThisWeek: [],
                redemptionsThisMonth: [],
                entityCount: {
                  customerCount: 20,
                  outletCount: 100,
                  userCount: 101,
                },
              },
            }),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
