import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-detailed-redemptions-chart',
  templateUrl: './detailed-redemptions-chart.component.html',
  styleUrls: ['./detailed-redemptions-chart.component.css'],
})
export class DetailedRedemptionsChartComponent implements OnInit, OnDestroy {
  @Input()
  detailedRedemptionsInfo: any;

  // chart options
  colorScheme: any;
  currentTheme: string;
  data = [];
  xAxis = true;
  yAxis = true;
  showXAxisLabel = true;
  showYAxisLabel = true;
  showGridLines = false;
  barPadding = 16;
  xAxisLabel = 'Redemptions today';

  private themeSub: Subscription;

  constructor(private themeService: ThemeService) {}

  ngOnInit() {
    this.themeSub = this.themeService.theme$.subscribe(value => {
      if (value === 'blue') {
        this.colorScheme = {
          domain: ['#FFCDD2'],
        };
      } else if (value === 'red') {
        this.colorScheme = {
          domain: ['#C5CAE9'],
        };
      } else {
        this.colorScheme = {
          domain: ['#CFD8DC'],
        };
      }
    });

    this.data = [
      {
        value: this.detailedRedemptionsInfo[7] || 0,
        name: '7:00',
      },
      {
        value: this.detailedRedemptionsInfo[8] || 0,
        name: '8:00',
      },
      {
        value: this.detailedRedemptionsInfo[9] || 0,
        name: '9:00',
      },
      {
        value: this.detailedRedemptionsInfo[10] || 0,
        name: '10:00',
      },
      {
        value: this.detailedRedemptionsInfo[11] || 0,
        name: '11:00',
      },
      {
        value: this.detailedRedemptionsInfo[12] || 0,
        name: '12:00',
      },
      {
        value: this.detailedRedemptionsInfo[13] || 0,
        name: '13:00',
      },
      {
        value: this.detailedRedemptionsInfo[14] || 0,
        name: '14:00',
      },
      {
        value: this.detailedRedemptionsInfo[15] || 0,
        name: '15:00',
      },
      {
        value: this.detailedRedemptionsInfo[16] || 0,
        name: '16:00',
      },
      {
        value: this.detailedRedemptionsInfo[17] || 0,
        name: '17:00',
      },
      {
        value: this.detailedRedemptionsInfo[18] || 0,
        name: '18:00',
      },
    ];
  }

  ngOnDestroy() {
    this.themeSub.unsubscribe();
  }
}
