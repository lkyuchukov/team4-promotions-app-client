import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PrizeCount } from '../../common/models/dashboard/prizes-count';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-prize-chart',
  templateUrl: './prize-chart.component.html',
  styleUrls: ['./prize-chart.component.css'],
})
export class PrizeChartComponent implements OnInit, OnDestroy {
  @Input()
  prizeInfo: PrizeCount;

  // chart options
  dataSource = [];
  colorScheme: any;
  currentTheme: string;
  // view = [500, 200];
  label = 'Total prizes redeemed';

  private themeSub: Subscription;

  constructor(private themeService: ThemeService) {}

  ngOnInit() {
    this.themeSub = this.themeService.theme$.subscribe(value => {
      if (value === 'blue') {
        this.colorScheme = {
          domain: ['#FFCDD2', '#E57373', '#C62828'],
        };
      } else if (value === 'red') {
        this.colorScheme = {
          domain: ['#C5CAE9', '#3F51B5', '#1A237E'],
        };
      } else {
        this.colorScheme = {
          domain: ['#CFD8DC', '#607D8B', '#37474F'],
        };
      }
    });

    this.dataSource = [
      {
        name: 'Small prize',
        value: this.prizeInfo.smallPrizesCount,
      },
      {
        name: 'Medium prize',
        value: this.prizeInfo.mediumPrizesCount,
      },
      {
        name: 'Big Prize',
        value: this.prizeInfo.bigPrizesCount,
      },
    ];
  }

  ngOnDestroy() {
    this.themeSub.unsubscribe();
  }
}
