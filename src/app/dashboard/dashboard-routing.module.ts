import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolesGuard } from '../common/guards/roles.guard';
import { DashboardResolverService } from './dashboard-resolver.service';
import { DashboardComponent } from './dashboard.component';
import { AuthGuard } from '../common/guards/auth.guard';

const dashboardRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard, RolesGuard],
    resolve: {
      dashboardInfo: DashboardResolverService,
    },
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(dashboardRoutes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
