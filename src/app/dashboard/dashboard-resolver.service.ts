import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { DashboardService } from '../core/services/dashboard.service';
import { NotificatorService } from '../core/services/notificator.service';

@Injectable({
  providedIn: 'root',
})
export class DashboardResolverService implements Resolve<any> {
  constructor(
    private dashboardService: DashboardService,
    private notificator: NotificatorService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.dashboardService.getDashboardInfo().pipe(
      mergeMap(dashboardInfo => {
        if (dashboardInfo) {
          return of(dashboardInfo);
        }
      }),
      catchError(() => {
        this.notificator.openSnackbar('Sorry, something went wrong');
        return of([{}]);
      }),
    );
  }
}
