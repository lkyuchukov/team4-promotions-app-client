import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Activity } from '../common/models/activity/activity';
import { DetailedPrizeInfo } from '../common/models/dashboard/detailed-prize-info';
import { EntityCount } from '../common/models/dashboard/entity-count';
import { PrizeCount } from '../common/models/dashboard/prizes-count';
import { RedemptionRecord } from '../common/models/redemption-record/redemption-record';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  activity: Activity[];

  prizeInfo: PrizeCount;

  redemptionsToday: RedemptionRecord[];

  redemptionsThisWeek: RedemptionRecord[];

  redemptionsThisMonth: RedemptionRecord[];

  entityCount: EntityCount;

  detailedRedemptionsInfo: any;

  detailedPrizeInfo: DetailedPrizeInfo;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.subscribe(
      (data: {
        dashboardInfo: {
          activity: Activity[];
          prizeInfo: PrizeCount;
          redemptionsToday: RedemptionRecord[];
          redemptionsThisWeek: RedemptionRecord[];
          redemptionsThisMonth: RedemptionRecord[];
          entityCount: EntityCount;
          detailedRedemptionsInfo: any;
          detailedPrizeInfo: DetailedPrizeInfo;
        };
      }) => {
        this.activity = data.dashboardInfo.activity;
        this.prizeInfo = data.dashboardInfo.prizeInfo;
        this.redemptionsToday = data.dashboardInfo.redemptionsToday;
        this.redemptionsThisWeek = data.dashboardInfo.redemptionsThisWeek;
        this.redemptionsThisMonth = data.dashboardInfo.redemptionsThisMonth;
        this.entityCount = data.dashboardInfo.entityCount;
        this.detailedRedemptionsInfo =
          data.dashboardInfo.detailedRedemptionsInfo;
        this.detailedPrizeInfo = data.dashboardInfo.detailedPrizeInfo;
      },
    );
  }
}
