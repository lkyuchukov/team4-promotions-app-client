import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { RedemptionRecord } from '../../common/models/redemption-record/redemption-record';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-redemptions-chart',
  templateUrl: './redemptions-chart.component.html',
  styleUrls: ['./redemptions-chart.component.css'],
})
export class RedemptionsChartComponent implements OnInit, OnDestroy {
  @Input()
  redemptionsToday: RedemptionRecord[];

  @Input()
  redemptionsThisWeek: RedemptionRecord[];

  @Input()
  redemptionsThisMonth: RedemptionRecord[];

  // chart options
  colorScheme: any;
  currentTheme: string;
  data = [];
  barPadding = 24;
  xAxis = true;
  yAxis = true;
  showXAxisLabel = true;
  showYAxisLabel = true;
  showGridLines = false;
  xAxisLabel = 'Redemptions';

  private themeSub: Subscription;

  constructor(private themeService: ThemeService) {}

  ngOnInit() {
    this.themeSub = this.themeService.theme$.subscribe(value => {
      if (value === 'blue') {
        this.colorScheme = {
          domain: ['#FFCDD2', '#E57373', '#C62828'],
        };
      } else if (value === 'red') {
        this.colorScheme = {
          domain: ['#C5CAE9', '#3F51B5', '#1A237E'],
        };
      } else {
        this.colorScheme = {
          domain: ['#CFD8DC', '#607D8B', '#37474F'],
        };
      }
    });

    this.data = [
      {
        name: 'This month',
        value: this.redemptionsThisMonth.length,
        extra: { code: 'this month' },
      },
      {
        name: 'This week',
        value: this.redemptionsThisWeek.length,
        extra: { code: 'this week' },
      },
      {
        name: 'Today',
        value: this.redemptionsToday.length,
        extra: { code: 'today' },
      },
    ];
  }

  ngOnDestroy() {
    this.themeSub.unsubscribe();
  }
}
