import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Customer } from '../common/models/customer/customer';
import { Outlet } from '../common/models/outlet/outlet';

@Component({
  selector: 'app-outlet',
  templateUrl: './outlet.component.html',
  styleUrls: ['./outlet.component.css'],
})
export class OutletComponent implements OnInit {
  constructor(private route: ActivatedRoute) {}

  @Output()
  outlets: Outlet[];

  @Output()
  customers: Customer[];

  ngOnInit() {
    this.route.data.subscribe(
      (data: { outlets: Outlet[]; customers: Customer[] }) => {
        this.outlets = data.outlets;
      },
    );
  }
}
