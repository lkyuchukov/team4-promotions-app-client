import { Location } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { SharedModule } from '../../shared/shared.module';
import { AddUserComponent } from './add-user/add-user.component';
import { DeleteOutletComponent } from './delete-outlet/delete-outlet.component';
import { EditOutletComponent } from './edit-outlet/edit-outlet.component';
import { OutletDetailComponent } from './outlet-detail.component';
import { UserListComponent } from './user-list/user-list.component';

describe('OutletDetailComponent', () => {
  let component: OutletDetailComponent;
  let fixture: ComponentFixture<OutletDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule, BrowserAnimationsModule],
      declarations: [
        OutletDetailComponent,
        EditOutletComponent,
        AddUserComponent,
        DeleteOutletComponent,
        UserListComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              outlet: {
                id: '2ea3afce-fd99-4879-9298-8cebb8710836',
                isDeleted: false,
                customer: {},
                address: 'test address',
                city: 'test city',
                redemptionRecords: [],
                users: [],
              },
            }),
          },
        },
        { provide: Location, useValue: {} },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutletDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('outletInfo should be defined', () => {
    expect(component.outletInfo).toBeTruthy();
  });
});
