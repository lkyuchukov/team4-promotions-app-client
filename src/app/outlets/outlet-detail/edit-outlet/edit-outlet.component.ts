import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Outlet } from '../../../common/models/outlet/outlet';
import { EditOutletDialogComponent } from '../edit-outlet-dialog/edit-outlet-dialog.component';

@Component({
  selector: 'app-edit-outlet',
  templateUrl: './edit-outlet.component.html',
  styleUrls: ['./edit-outlet.component.css'],
})
export class EditOutletComponent implements OnInit {
  @Input()
  outletInfo: Outlet;

  constructor(public dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(EditOutletDialogComponent, {
      data: this.outletInfo,
      width: '450px',
    });
  }

  ngOnInit() {}
}
