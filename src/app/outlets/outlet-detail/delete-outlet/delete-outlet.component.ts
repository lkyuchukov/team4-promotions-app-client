import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Outlet } from '../../../common/models/outlet/outlet';
import { DeleteOutletDialogComponent } from '../delete-outlet-dialog/delete-outlet-dialog.component';

@Component({
  selector: 'app-delete-outlet',
  templateUrl: './delete-outlet.component.html',
  styleUrls: ['./delete-outlet.component.css'],
})
export class DeleteOutletComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  @Input()
  outletInfo: Outlet;

  ngOnInit() {}

  openDialog() {
    // apply css styles to dialog here
    const dialogRef = this.dialog.open(DeleteOutletDialogComponent, {
      data: this.outletInfo.id,
    });
  }
}
