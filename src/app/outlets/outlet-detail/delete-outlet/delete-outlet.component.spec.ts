import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteOutletComponent } from './delete-outlet.component';
import { SharedModule } from '../../../shared/shared.module';

describe('DeleteOutletComponent', () => {
  let component: DeleteOutletComponent;
  let fixture: ComponentFixture<DeleteOutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [DeleteOutletComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
