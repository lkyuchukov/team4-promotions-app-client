import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NotificatorService } from '../../../core/services/notificator.service';
import { OutletService } from '../../../core/services/outlet.service';
import { EditOutletDialogComponent } from './edit-outlet-dialog.component';
import { SharedModule } from '../../../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('EditOutletDialogComponent', () => {
  let component: EditOutletDialogComponent;
  let fixture: ComponentFixture<EditOutletDialogComponent>;
  const formBuilder = new FormBuilder();

  beforeEach(async(() => {
    const notificatorServiceSpy = jasmine.createSpyObj('NotificatorService', [
      'openSnackbar',
    ]);
    const outletServiceSpy = jasmine.createSpyObj('OutletService', [
      'editOutlet',
    ]);

    TestBed.configureTestingModule({
      imports: [SharedModule, BrowserAnimationsModule],
      declarations: [EditOutletDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: NotificatorService,
          useValue: notificatorServiceSpy,
        },
        {
          provide: OutletService,
          useValue: outletServiceSpy,
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOutletDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
