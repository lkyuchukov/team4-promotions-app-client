import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Outlet } from '../../../common/models/outlet/outlet';
import { NotificatorService } from '../../../core/services/notificator.service';
import { OutletService } from '../../../core/services/outlet.service';

@Component({
  selector: 'app-edit-outlet-dialog',
  templateUrl: './edit-outlet-dialog.component.html',
  styleUrls: ['./edit-outlet-dialog.component.css'],
})
export class EditOutletDialogComponent implements OnInit {
  editOutletForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private notificator: NotificatorService,
    private outletService: OutletService,
    @Inject(MAT_DIALOG_DATA) public data: Outlet,
  ) {}

  ngOnInit() {
    this.editOutletForm = this.formBuilder.group({
      address: [`${this.data.address}`, [Validators.required]],
      city: [`${this.data.city}`, Validators.required],
    });
  }

  editOutlet() {
    const outletData = this.editOutletForm.value;
    this.outletService.editOutlet(this.data.id, outletData).subscribe(
      res => {
        this.data.address = res.address;
        this.data.city = res.city;
        this.notificator.openSnackbar('Outlet data edited.');
      },
      () => {
        this.notificator.openSnackbar('Sorry, something went wrong.');
      },
    );
  }
}
