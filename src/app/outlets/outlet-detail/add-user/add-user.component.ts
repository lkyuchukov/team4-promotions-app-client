import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Outlet } from '../../../common/models/outlet/outlet';
import { AddUserDialogComponent } from '../add-user-dialog/add-user-dialog.component';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
})
export class AddUserComponent implements OnInit {
  @Input()
  outletInfo: Outlet;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  openDialog() {
    const dialogRef = this.dialog.open(AddUserDialogComponent, {
      width: '450px',
      data: this.outletInfo,
    });
  }
}
