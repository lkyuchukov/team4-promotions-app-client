import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { NotificatorService } from '../../../core/services/notificator.service';
import { OutletService } from '../../../core/services/outlet.service';

@Component({
  selector: 'app-delete-outlet-dialog',
  templateUrl: './delete-outlet-dialog.component.html',
  styleUrls: ['./delete-outlet-dialog.component.css'],
})
export class DeleteOutletDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DeleteOutletDialogComponent>,
    private outletService: OutletService,
    private notificator: NotificatorService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: string,
  ) {}

  ngOnInit() {}

  deleteOutlet() {
    this.outletService.deleteOutlet(this.data).subscribe(
      () => {
        this.router.navigate(['/outlets']);
        this.notificator.openSnackbar('Outlet deleted.');
      },
      () => {
        this.notificator.openSnackbar('Sorry, something went wrong.');
      },
    );
  }
}
