import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NotificatorService } from '../../../core/services/notificator.service';
import { OutletService } from '../../../core/services/outlet.service';
import { SharedModule } from '../../../shared/shared.module';
import { DeleteOutletDialogComponent } from './delete-outlet-dialog.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('DeleteOutletDialogComponent', () => {
  let component: DeleteOutletDialogComponent;
  let fixture: ComponentFixture<DeleteOutletDialogComponent>;

  beforeEach(async(() => {
    const notificatorServiceSpy = jasmine.createSpyObj('NotificatorService', [
      'openSnackbar',
    ]);
    const outletServiceSpy = jasmine.createSpyObj('OutletService', [
      'deleteOutlet',
    ]);

    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule],
      declarations: [DeleteOutletDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: NotificatorService,
          useValue: notificatorServiceSpy,
        },
        {
          provide: OutletService,
          useValue: outletServiceSpy,
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteOutletDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
