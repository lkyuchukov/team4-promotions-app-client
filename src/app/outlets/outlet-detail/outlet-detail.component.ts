import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Outlet } from '../../common/models/outlet/outlet';

@Component({
  selector: 'app-outlet-detail',
  templateUrl: './outlet-detail.component.html',
  styleUrls: ['./outlet-detail.component.css'],
})
export class OutletDetailComponent implements OnInit {
  constructor(private route: ActivatedRoute) {}

  outletInfo: Outlet;

  ngOnInit() {
    this.route.data.subscribe((data: { outlet: Outlet }) => {
      this.outletInfo = data.outlet;
    });
  }
}
