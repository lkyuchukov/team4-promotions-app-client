import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { of } from 'rxjs';
import { catchError, mergeMap, take } from 'rxjs/operators';
import { Outlet } from '../../common/models/outlet/outlet';
import { OutletService } from '../../core/services/outlet.service';
import { NotificatorService } from '../../core/services/notificator.service';

@Injectable({
  providedIn: 'root',
})
export class OutletDetailResolverService implements Resolve<Outlet> {
  constructor(
    private outletService: OutletService,
    private notificator: NotificatorService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const id = route.paramMap.get('id');

    return this.outletService.getOutlet(id).pipe(
      // this ensures the observable completes after retrieving the first value
      take(1),
      mergeMap(outlet => {
        if (outlet) {
          return of(outlet);
        }
      }),
      catchError(() => {
        this.notificator.openSnackbar('Failed to get outlet data.');
        return of({});
      }),
    );
  }
}
