import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddUserDialogComponent } from './add-user-dialog.component';
import { FormBuilder } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserService } from '../../../core/services/user.service';
import { NotificatorService } from '../../../core/services/notificator.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

describe('AddUserDialogComponent', () => {
  let component: AddUserDialogComponent;
  let fixture: ComponentFixture<AddUserDialogComponent>;
  const formBuilder = new FormBuilder();

  beforeEach(async(() => {
    const notificatorSpy = jasmine.createSpyObj('NotificatorService', [
      'openSnackbar',
    ]);
    const userServiceSpy = jasmine.createSpyObj('UserService', ['addUser']);

    TestBed.configureTestingModule({
      imports: [SharedModule, BrowserAnimationsModule],
      declarations: [AddUserDialogComponent],
      providers: [
        { provide: FormBuilder, useValue: formBuilder },
        {
          provide: UserService,
          useValue: userServiceSpy,
        },
        {
          provide: NotificatorService,
          useValue: notificatorSpy,
        },
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
