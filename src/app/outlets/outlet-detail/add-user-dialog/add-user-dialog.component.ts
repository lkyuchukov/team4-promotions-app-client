import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Outlet } from '../../../common/models/outlet/outlet';
import { NotificatorService } from '../../../core/services/notificator.service';
import { UserService } from '../../../core/services/user.service';

@Component({
  selector: 'app-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.css'],
})
export class AddUserDialogComponent implements OnInit {
  addUserForm: FormGroup;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private notificator: NotificatorService,
    @Inject(MAT_DIALOG_DATA) public data: Outlet,
  ) {}

  ngOnInit() {
    this.addUserForm = this.formBuilder.group({
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(25),
        ],
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(25),
        ],
      ],
    });
  }

  addUser() {
    const userData = this.addUserForm.value;
    userData.outletId = this.data.id;
    this.userService.addUser(userData).subscribe(
      res => {
        this.data.users.push(res);
        this.notificator.openSnackbar('User added.');
      },
      err => {
        this.notificator.openSnackbar(err.error.message);
      },
    );
  }
}
