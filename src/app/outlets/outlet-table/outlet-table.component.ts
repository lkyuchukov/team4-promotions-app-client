import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Outlet } from '../../common/models/outlet/outlet';
import { SearchService } from '../../core/services/search.service';

@Component({
  selector: 'app-outlet-table',
  templateUrl: './outlet-table.component.html',
  styleUrls: ['./outlet-table.component.css'],
})
export class OutletTableComponent implements OnInit {
  @Input()
  outlets: Outlet[];

  displayedColumns: string[] = [
    'address',
    'city',
    'customer',
    'employees',
    'detail',
  ];

  dataSource: MatTableDataSource<Outlet>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private router: Router, private searchService: SearchService) {}

  // custom filter function for searching nested objects
  applyFilter(filterValue: string) {
    this.dataSource.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm: string, key: string) => {
        return this.searchService.nestedFilterCheck(currentTerm, data, key);
      };
      const dataStr = Object.keys(data)
        .reduce(accumulator, '')
        .toLowerCase();
      // Transform the filter by converting it to lowercase and removing whitespace.
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
    this.dataSource.filter = filterValue;
  }

  goToOutletDetail(id: string) {
    this.router.navigate([`outlets/${id}`]);
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.outlets);
    this.dataSource.paginator = this.paginator;
  }
}
