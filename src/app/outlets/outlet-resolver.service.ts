import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { Outlet } from '../common/models/outlet/outlet';
import { NotificatorService } from '../core/services/notificator.service';
import { OutletService } from '../core/services/outlet.service';

@Injectable({
  providedIn: 'root',
})
export class OutletResolverService implements Resolve<Outlet[]> {
  constructor(
    private outletService: OutletService,
    private notificator: NotificatorService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.outletService.getAllOutlets().pipe(
      mergeMap(outlets => {
        if (outlets) {
          return of(outlets);
        }
      }),
      catchError(() => {
        this.notificator.openSnackbar('Sorry, something went wrong');
        return of([{}]);
      }),
    );
  }
}
