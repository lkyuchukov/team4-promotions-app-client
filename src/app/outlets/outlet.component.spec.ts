import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { SharedModule } from '../shared/shared.module';
import { OutletTableComponent } from './outlet-table/outlet-table.component';
import { OutletComponent } from './outlet.component';

describe('OutletComponent', () => {
  let component: OutletComponent;
  let fixture: ComponentFixture<OutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule, BrowserAnimationsModule],
      declarations: [OutletComponent, OutletTableComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              outlets: [
                {
                  id: '2ea3afce-fd99-4879-9298-8cebb8710836',
                  isDeleted: false,
                  customer: {},
                  address: 'test address',
                  city: 'test city',
                  redemptionRecords: [],
                  users: [],
                },
              ],
            }),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('outlets should be defined', () => {
    expect(component.outlets).toBeTruthy();
  });
});
