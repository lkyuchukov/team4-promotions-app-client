import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../common/guards/auth.guard';
import { RolesGuard } from '../common/guards/roles.guard';
import { CustomerResolverService } from '../customers/customer-resolver.service';
import { OutletDetailResolverService } from './outlet-detail/outlet-detail-resolver.service';
import { OutletDetailComponent } from './outlet-detail/outlet-detail.component';
import { OutletResolverService } from './outlet-resolver.service';
import { OutletComponent } from './outlet.component';

const outletRoutes: Routes = [
  {
    path: '',
    component: OutletComponent,
    canActivate: [AuthGuard, RolesGuard],
    resolve: {
      outlets: OutletResolverService,
      customers: CustomerResolverService,
    },
  },
  {
    path: ':id',
    component: OutletDetailComponent,
    canActivate: [AuthGuard, RolesGuard],
    resolve: {
      outlet: OutletDetailResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(outletRoutes)],
  exports: [RouterModule],
})
export class OutletsRoutingModule {}
