import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AddUserDialogComponent } from './outlet-detail/add-user-dialog/add-user-dialog.component';
import { AddUserComponent } from './outlet-detail/add-user/add-user.component';
import { DeleteOutletDialogComponent } from './outlet-detail/delete-outlet-dialog/delete-outlet-dialog.component';
import { DeleteOutletComponent } from './outlet-detail/delete-outlet/delete-outlet.component';
import { EditOutletDialogComponent } from './outlet-detail/edit-outlet-dialog/edit-outlet-dialog.component';
import { EditOutletComponent } from './outlet-detail/edit-outlet/edit-outlet.component';
import { OutletDetailComponent } from './outlet-detail/outlet-detail.component';
import { UserListComponent } from './outlet-detail/user-list/user-list.component';
import { OutletTableComponent } from './outlet-table/outlet-table.component';
import { OutletComponent } from './outlet.component';
import { OutletsRoutingModule } from './outlets-routing.module';

@NgModule({
  entryComponents: [
    DeleteOutletDialogComponent,
    EditOutletDialogComponent,
    AddUserDialogComponent,
  ],
  declarations: [
    OutletComponent,
    OutletDetailComponent,
    EditOutletComponent,
    DeleteOutletComponent,
    DeleteOutletDialogComponent,
    OutletTableComponent,
    UserListComponent,
    AddUserComponent,
    EditOutletDialogComponent,
    AddUserDialogComponent,
  ],
  imports: [SharedModule, OutletsRoutingModule],
})
export class OutletsModule {}
