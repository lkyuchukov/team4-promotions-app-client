import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { HelpdeskComponent } from './components/helpdesk/helpdesk.component';
import { LoadingScreenComponent } from './components/loading-screen/loading-screen.component';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/navigation/admin/admin.component';
import { NavbarComponent } from './components/navigation/navbar.component';
import { ThemePickerComponent } from './components/navigation/theme-picker/theme-picker.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AuthService } from './core/services/auth.service';
import { ThemeService } from './core/services/theme.service';
import { DashboardModule } from './dashboard/dashboard.module';
import { SharedModule } from './shared/shared.module';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  const authServiceStub = {
    user$: of({ username: 'test', role: [] }),
    logout: () => {},
  };

  const themeServiceStub = {
    theme$: of('blue'),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        SharedModule,
        DashboardModule,
      ],
      declarations: [
        AppComponent,
        LoginComponent,
        NavbarComponent,
        HelpdeskComponent,
        AdminComponent,
        PageNotFoundComponent,
        LoadingScreenComponent,
        ThemePickerComponent,
      ],
      providers: [
        { provide: AuthService, useValue: authServiceStub },
        { provide: ThemeService, useValue: themeServiceStub },
      ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the app', () => {
    fixture.detectChanges();
    expect(component.username).toBeTruthy();
  });

  it('should initialize with the correct username when the user is logged in', () => {
    fixture.detectChanges();
    expect(component.username).toBe('test');
  });

  it('currentTheme should be blue by default', () => {
    fixture.detectChanges();
    expect(component.currentTheme).toBe('blue');
  });

  it('#logout should call #authService.logout()', () => {
    const service = fixture.debugElement.injector.get(AuthService);
    spyOn(service, 'logout').and.callThrough();
    component.logout();
    fixture.detectChanges();
    expect(service.logout).toHaveBeenCalled();
  });
});
