import { Component, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from './core/services/auth.service';
import { ThemeService } from './core/services/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  @Output()
  username = '';

  isLogged = false;

  role: string;

  currentTheme: string;

  userSub: Subscription;

  private themeSub: Subscription;

  constructor(
    private authService: AuthService,
    private router: Router,
    private themeService: ThemeService,
  ) {}

  ngOnInit() {
    this.userSub = this.authService.user$.subscribe(data => {
      if (data === null) {
        this.username = '';
        this.isLogged = false;
        this.role = '';
        this.router.navigate(['login']);
      } else {
        this.username = data.username;
        this.isLogged = true;
        this.role = data.role;
      }
    });

    this.themeSub = this.themeService.theme$.subscribe(value => {
      this.currentTheme = value;
    });

    console.log('testing git migration');
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
    this.themeSub.unsubscribe();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }
}
