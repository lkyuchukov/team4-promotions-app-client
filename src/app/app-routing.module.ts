import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HelpdeskComponent } from './components/helpdesk/helpdesk.component';
import { LoginComponent } from './components/login/login.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';

const routes: Routes = [
  {
    path: 'redemptions',
    loadChildren: './redemptions/redemptions.module#RedemptionsModule',
  },
  { path: 'users', loadChildren: './users/users.module#UsersModule' },
  { path: 'outlets', loadChildren: './outlets/outlets.module#OutletsModule' },
  {
    path: 'customers',
    loadChildren: './customers/customers.module#CustomersModule',
  },
  { path: 'login', component: LoginComponent },
  { path: 'helpdesk', component: HelpdeskComponent },
  { path: 'server-error', component: ServerErrorComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
