import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { RedemptionRecord } from '../../common/models/redemption-record/redemption-record';

@Component({
  selector: 'app-redemptions',
  templateUrl: './redemptions.component.html',
  styleUrls: ['./redemptions.component.css'],
})
export class RedemptionsComponent implements OnInit {
  redemptionRecords: RedemptionRecord[];

  displayedColumns: string[] = [
    'redeemedOn',
    'prize',
    'customer',
    'outlet',
    'user',
    'detail',
  ];

  dataSource: MatTableDataSource<RedemptionRecord>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  constructor(private route: ActivatedRoute, private router: Router) {}

  // custom filter function for searching nested objects
  applyFilter(filterValue: string) {
    this.dataSource.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm: string, key: string) => {
        return this.nestedFilterCheck(currentTerm, data, key);
      };
      const dataStr = Object.keys(data)
        .reduce(accumulator, '')
        .toLowerCase();
      // Transform the filter by converting it to lowercase and removing whitespace.
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
    this.dataSource.filter = filterValue;
  }

  nestedFilterCheck(search: string, data: any, key: string) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);
        }
      }
    } else {
      search += data[key];
    }
    return search;
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: { redemptionRecords: RedemptionRecord[] }) => {
        this.redemptionRecords = data.redemptionRecords;
        this.dataSource = new MatTableDataSource(this.redemptionRecords);
        this.dataSource.paginator = this.paginator;

        this.dataSource.sortingDataAccessor = (item, property) => {
          switch (property) {
            case 'redeemedOn':
              return new Date(item.redeemedOn);
            default:
              return item[property];
          }
        };

        this.dataSource.sort = this.sort;
      },
    );
  }

  goToUserDetail(id: string) {
    this.router.navigate([`users/${id}`]);
  }
}
