import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { SharedModule } from '../../shared/shared.module';
import { RedemptionsComponent } from './redemptions.component';

describe('RedemptionsComponent', () => {
  let component: RedemptionsComponent;
  let fixture: ComponentFixture<RedemptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule, BrowserAnimationsModule],
      declarations: [RedemptionsComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              redemptionRecords: [
                {
                  id: '2ea3afce-fd99-4879-9298-8cebb8710836',
                  createdOn: new Date(Date.now()),
                  outlet: {},
                  isDeleted: false,
                  redeemedOn: new Date(Date.now()),
                  user: {},
                  customer: {},
                },
              ],
            }),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedemptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('redemptionRecords should be defined', () => {
    expect(component.redemptionRecords).toBeTruthy();
  });
});
