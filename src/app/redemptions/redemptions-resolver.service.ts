import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { CodeService } from '../core/services/code.service';
import { NotificatorService } from '../core/services/notificator.service';
import { RedemptionRecord } from '../common/models/redemption-record/redemption-record';

@Injectable({
  providedIn: 'root',
})
export class RedemptionsResolverService implements Resolve<RedemptionRecord[]> {
  constructor(
    private codeService: CodeService,
    private notificator: NotificatorService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.codeService.getAllRedeemedCodes().pipe(
      mergeMap(redemptionRecords => {
        if (redemptionRecords) {
          return of(redemptionRecords);
        }
      }),
      catchError(() => {
        this.notificator.openSnackbar('Failed to get redemptions data.');
        return of([{}]);
      }),
    );
  }
}
