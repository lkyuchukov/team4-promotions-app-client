import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RedemptionsRoutingModule } from './redemptions-routing.module';
import { RedemptionsComponent } from './redemptions/redemptions.component';

@NgModule({
  declarations: [RedemptionsComponent],
  imports: [SharedModule, RedemptionsRoutingModule],
})
export class RedemptionsModule {}
