import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../common/guards/auth.guard';
import { RedemptionsResolverService } from './redemptions-resolver.service';
import { RedemptionsComponent } from './redemptions/redemptions.component';

const redemptionRoutes: Routes = [
  {
    path: '',
    component: RedemptionsComponent,
    canActivate: [AuthGuard],
    resolve: {
      redemptionRecords: RedemptionsResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(redemptionRoutes)],
  exports: [RouterModule],
})
export class RedemptionsRoutingModule {}
