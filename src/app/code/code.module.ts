import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CheckCodeComponent } from './check-code/check-code.component';
import { CodeRoutingModule } from './code-routing.module';
import { RedeemDialogComponent } from './redeem-dialog/redeem-dialog.component';

@NgModule({
  entryComponents: [RedeemDialogComponent],
  declarations: [CheckCodeComponent, RedeemDialogComponent],
  imports: [SharedModule, CodeRoutingModule],
})
export class CodeModule {}
