import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../common/guards/auth.guard';
import { CheckCodeComponent } from './check-code/check-code.component';

const codeRoutes: Routes = [
  { path: 'codes', component: CheckCodeComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(codeRoutes)],
  exports: [RouterModule],
})
export class CodeRoutingModule {}
