import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Code } from '../../common/models/code/code';
import { CodeService } from '../../core/services/code.service';
import { RedeemDialogComponent } from '../redeem-dialog/redeem-dialog.component';

@Component({
  selector: 'app-check-code',
  templateUrl: './check-code.component.html',
  styleUrls: ['./check-code.component.css'],
})
export class CheckCodeComponent implements OnInit {
  checkCode = new FormControl('');

  code: Code;

  constructor(private codeService: CodeService, public dialog: MatDialog) {}

  openDialog() {
    const codeValue = this.checkCode.value;
    this.checkIfCodeIsValid(codeValue).subscribe(data => {
      this.code = data;

      const dialogRef = this.dialog.open(RedeemDialogComponent, {
        data: {
          value: codeValue,
          status: this.code.status,
          prize: this.code.prize,
        },
      });
    });
  }

  ngOnInit() {
    this.checkCode.setValidators([
      Validators.required,
      Validators.minLength(13),
      Validators.maxLength(13),
    ]);
  }

  checkIfCodeIsValid(code: string) {
    return this.codeService.checkIfCodeIsValid(code);
  }
}
