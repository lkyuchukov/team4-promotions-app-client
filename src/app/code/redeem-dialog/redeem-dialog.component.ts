import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import { Code } from '../../common/models/code/code';
import { CodeService } from '../../core/services/code.service';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-redeem-dialog',
  templateUrl: './redeem-dialog.component.html',
  styleUrls: ['./redeem-dialog.component.css'],
})
export class RedeemDialogComponent implements OnInit {
  constructor(
    private codeService: CodeService,
    private notificator: NotificatorService,
    public dialogRef: MatDialogRef<RedeemDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  codeInfo: Code;

  themeSubscription: Subscription;

  ngOnInit() {
    this.codeInfo = this.data;
  }

  reportMultipleRedemptionAttempt() {
    return this.codeService
      .reportMultipleRedemptionAttempt(this.codeInfo)
      .subscribe(
        () => {
          this.notificator.openSnackbar('Reported.');
        },
        () => {
          this.notificator.openSnackbar('Sorry, something went wrong');
        },
      );
  }

  redeemCode(codeValue: string) {
    return this.codeService.redeemCode(codeValue).subscribe(
      () => {
        this.notificator.openSnackbar('Code has been marked as redeemed.');
      },
      () => {
        this.notificator.openSnackbar('Sorry, something went wrong.');
      },
    );
  }
}
