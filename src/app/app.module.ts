import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CodeModule } from './code/code.module';
import { ErrorInterceptor } from './common/interceptors/error.interceptor';
import { LoadingScreenInterceptor } from './common/interceptors/loading.interceptor';
import { TokenInterceptorService } from './common/interceptors/token-interceptor.service';
import { HelpdeskComponent } from './components/helpdesk/helpdesk.component';
import { LoadingScreenComponent } from './components/loading-screen/loading-screen.component';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/navigation/admin/admin.component';
import { NavbarComponent } from './components/navigation/navbar.component';
import { ThemePickerComponent } from './components/navigation/theme-picker/theme-picker.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { CoreModule } from './core/core.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    HelpdeskComponent,
    AdminComponent,
    PageNotFoundComponent,
    LoadingScreenComponent,
    ThemePickerComponent,
    ServerErrorComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    CoreModule,
    CodeModule,
    DashboardModule,
    AppRoutingModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingScreenInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
