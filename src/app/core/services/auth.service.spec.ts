import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { AuthService } from './auth.service';
import { StorageService } from './storage.service';

describe('AuthService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete']);
  const storage = jasmine.createSpyObj('StorageService', [
    'get',
    'set',
    'remove',
  ]);

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: http,
        },
        {
          provide: StorageService,
          useValue: storage,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });

  it('#login() should log the user in', () => {
    http.post.and.returnValue(
      of({
        token: 'token',
        user: {
          username: 'testuser',
          id: '2e679030-9d12-473d-b6e6-902116093926',
          roles: [
            {
              id: '54957d43-a4ed-40d8-8e89-c93665eff516',
              name: 'Basic',
            },
          ],
          createdOn: new Date(Date.now()),
          isLoggedIn: true,
          isDeleted: false,
        },
      }),
    );
    const service: AuthService = TestBed.get(AuthService);

    const userObj = {
      username: 'testuser',
      password: 'password',
    };

    storage.set.calls.reset();
    service.login(userObj).subscribe(res => {
      expect(res.user.username).toBe('testuser');
    });
  });

  it('#login() should call #http.post()', () => {
    const service: AuthService = TestBed.get(AuthService);

    const userObj = {
      username: 'testuser',
      password: 'password',
    };

    http.post.calls.reset();
    storage.set.calls.reset();
    service
      .login(userObj)
      .subscribe(() => expect(http.post).toHaveBeenCalledTimes(1));
  });

  it('#login() should call #storage.set() 3 times', () => {
    http.post.and.returnValue(
      of({
        token: 'token',
        user: {
          username: 'testuser1',
          id: '2e679030-9d12-473d-b6e6-902116093926',
          roles: [
            {
              id: '54957d43-a4ed-40d8-8e89-c93665eff516',
              name: 'Basic',
            },
          ],
          createdOn: new Date(Date.now()),
          isLoggedIn: true,
          isBanned: false,
        },
      }),
    );
    const service: AuthService = TestBed.get(AuthService);

    const userObj = {
      username: 'testuser',
      password: 'password',
    };

    service.login(userObj);

    expect(storage.set).toHaveBeenCalledTimes(3);
    expect(storage.set).toHaveBeenCalledWith('token', 'token');
    expect(storage.set).toHaveBeenCalledWith('username', 'testuser');
  });

  it('#login() should update the subject', () => {
    http.post.and.returnValue(
      of({
        token: 'token',
        user: {
          username: 'testuser1',
          roles: [
            {
              id: '54957d43-a4ed-40d8-8e89-c93665eff516',
              name: 'Basic',
            },
          ],
          createdOn: new Date(Date.now()),
          isLoggedIn: true,
          isDeleted: false,
        },
      }),
    );

    const service: AuthService = TestBed.get(AuthService);

    const userObj = {
      username: 'testuser1',
      password: 'password',
    };

    service.login(userObj).subscribe(() => {
      service.user$.subscribe(user => expect(user.username).toBe('testuser1'));
    });
  });
  it('#logout() should change the subject to <null>', () => {
    http.delete.and.returnValue(
      of({
        message: 'Successful logout.',
      }),
    );

    const service: AuthService = TestBed.get(AuthService);

    service.logout().subscribe(() => {
      service.user$.subscribe(username => expect(username).toBe(null));
    });
  });

  it('#logout() should call #storage.remove() 3 times', () => {
    const service: AuthService = TestBed.get(AuthService);

    storage.remove.calls.reset();

    service.logout();

    expect(storage.remove).toHaveBeenCalledTimes(3);
    expect(storage.remove).toHaveBeenCalledWith('token');
    expect(storage.remove).toHaveBeenCalledWith('username');
  });
});
