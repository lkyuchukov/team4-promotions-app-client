import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Code } from '../../common/models/code/code';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CodeService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  checkIfCodeIsValid(code: string) {
    return this.http.get<any>(`${this.baseUrl}/codes/${code}`);
  }

  redeemCode(code: string) {
    return this.http.post<any>(`${this.baseUrl}/codes/`, { code });
  }

  getAllRedeemedCodes() {
    return this.http.get<any>(`${this.baseUrl}/codes`);
  }

  reportMultipleRedemptionAttempt(code: Code) {
    return this.http.post<void>(`${this.baseUrl}/reports`, code);
  }
}
