import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { IssueService } from './issue.service';

describe('IssueService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['post']);

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: http,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: IssueService = TestBed.get(IssueService);
    expect(service).toBeTruthy();
  });
});
