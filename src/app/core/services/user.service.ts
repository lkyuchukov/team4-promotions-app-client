import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateUser } from '../../common/models/user/create-user';
import { UpdateUser } from '../../common/models/user/update-user';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getAllUsers() {
    return this.http.get<any>(`${this.baseUrl}/users`);
  }

  getUser(id: string) {
    return this.http.get<any>(`${this.baseUrl}/users/${id}`);
  }

  addUser(userData: CreateUser) {
    return this.http.post<any>(`${this.baseUrl}/users`, userData);
  }

  editUser(id: string, userData: Partial<UpdateUser>) {
    return this.http.put<any>(`${this.baseUrl}/users/${id}`, userData);
  }

  deleteUser(id: string) {
    return this.http.delete<any>(`${this.baseUrl}/users/${id}`);
  }
}
