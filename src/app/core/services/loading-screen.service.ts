import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoadingScreenService {
  private loadingStatusSubject = new Subject<boolean>();

  getLoadingStatus() {
    return this.loadingStatusSubject.asObservable();
  }

  startLoading() {
    this.loadingStatusSubject.next(true);
  }

  stopLoading() {
    this.loadingStatusSubject.next(false);
  }
}
