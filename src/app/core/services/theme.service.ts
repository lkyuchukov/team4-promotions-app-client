import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  private currentTheme = new BehaviorSubject<string>(this.getPrefTheme());

  theme$ = this.currentTheme.asObservable();

  constructor(private storage: StorageService) {}

  setBlueTheme() {
    this.storage.set('prefTheme', 'blue');
    this.currentTheme.next('blue');
  }

  setRedTheme() {
    this.storage.set('prefTheme', 'red');
    this.currentTheme.next('red');
  }

  setLightTheme() {
    this.storage.set('prefTheme', 'light');
    this.currentTheme.next('light');
  }

  getPrefTheme() {
    const theme = this.storage.get('prefTheme');
    if (theme) {
      return theme;
    }
    return 'blue';
  }
}
