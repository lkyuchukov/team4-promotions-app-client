import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { CreateIssue } from '../../common/models/issue/create-issue';

@Injectable({
  providedIn: 'root',
})
export class IssueService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  submitIssue(issue: CreateIssue) {
    return this.http.post<CreateIssue>(`${this.baseUrl}/issues`, issue);
  }
}
