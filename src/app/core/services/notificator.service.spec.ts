import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material';
import { NotificatorService } from './notificator.service';

describe('NotificatorService', () => {
  const snackbar = jasmine.createSpyObj('NotificatorService', ['openSnackbar']);
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatSnackBar,
          useValue: snackbar,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: NotificatorService = TestBed.get(NotificatorService);
    expect(service).toBeTruthy();
  });
});
