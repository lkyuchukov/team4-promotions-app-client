import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateCustomer } from '../../common/models/customer/create-customer';
import { UpdateCustomer } from '../../common/models/customer/update-customer';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getAllCustomers() {
    return this.http.get<any>(`${this.baseUrl}/customers`);
  }

  getCustomer(id: string) {
    return this.http.get<any>(`${this.baseUrl}/customers/${id}`);
  }

  addCustomer(customerData: CreateCustomer) {
    return this.http.post<any>(`${this.baseUrl}/customers`, customerData);
  }

  editCustomer(id: string, customerData: UpdateCustomer) {
    return this.http.put<any>(`${this.baseUrl}/customers/${id}`, customerData);
  }

  deleteCustomer(id: string) {
    return this.http.delete<any>(`${this.baseUrl}/customers/${id}`);
  }
}
