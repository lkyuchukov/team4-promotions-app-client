import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateOutlet } from '../../common/models/outlet/create-outlet';
import { UpdateOutlet } from '../../common/models/outlet/update-outlet';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OutletService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  public getAllOutlets() {
    return this.http.get<any>(`${this.baseUrl}/outlets`);
  }

  public getOutlet(id: string) {
    return this.http.get<any>(`${this.baseUrl}/outlets/${id}`);
  }

  public addOutlet(outletData: CreateOutlet) {
    return this.http.post<any>(`${this.baseUrl}/outlets/`, outletData);
  }

  public editOutlet(id: string, outletData: UpdateOutlet) {
    return this.http.put<any>(`${this.baseUrl}/outlets/${id}`, outletData);
  }

  public deleteOutlet(id: string) {
    return this.http.delete<any>(`${this.baseUrl}/outlets/${id}`);
  }
}
