import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { UserService } from './user.service';

describe('UserService', () => {
  const http = jasmine.createSpyObj('HttpClient', [
    'get',
    'post',
    'put',
    'delete',
  ]);

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: http,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('#getUser() should return the user', () => {
    const service: UserService = TestBed.get(UserService);
    const username = 'testuser';

    http.get.and.returnValue(
      of({
        id: 'b7552156-1035-41ca-bbef-484d19affe94',
        username: 'testuser',
        roles: [
          {
            id: '1a9b18fc-74c0-417a-a3cc-daa03b5062a8',
            name: 'Basic',
          },
        ],
        createdOn: new Date(Date.now()),
        isLoggedIn: true,
        isDeleted: false,
        redemptionRecords: [],
        outlet: {},
      }),
    );

    service.getUser(username).subscribe(res => {
      expect(res.username).toBe('testuser');
    });
    http.get.calls.reset();
  });

  it('#getAllUsers() should return an array of users', () => {
    const service: UserService = TestBed.get(UserService);
    const users = [
      {
        id: 'b7552156-1035-41ca-bbef-484d19affe94',
        username: 'testuser1',
        roles: [
          {
            id: '1a9b18fc-74c0-417a-a3cc-daa03b5062a8',
            name: 'Basic',
          },
        ],
        createdOn: new Date(Date.now()),
        isLoggedIn: true,
        isDeleted: false,
        redemptionRecords: [],
        outlet: {},
      },
      {
        id: 'b7552156-1035-41ca-bbef-484d19affe94',
        username: 'testuser2',
        roles: [
          {
            id: '1a9b18fc-74c0-417a-a3cc-daa03b5062a8',
            name: 'Basic',
          },
        ],
        createdOn: new Date(Date.now()),
        isLoggedIn: true,
        isDeleted: false,
        redemptionRecords: [],
        outlet: {},
      },
    ];

    http.get.and.returnValue(
      of([
        {
          id: 'b7552156-1035-41ca-bbef-484d19affe94',
          username: 'testuser1',
          roles: [
            {
              id: '1a9b18fc-74c0-417a-a3cc-daa03b5062a8',
              name: 'Basic',
            },
          ],
          createdOn: new Date(Date.now()),
          isLoggedIn: true,
          isDeleted: false,
          redemptionRecords: [],
          outlet: {},
        },
        {
          id: 'b7552156-1035-41ca-bbef-484d19affe94',
          username: 'testuser2',
          roles: [
            {
              id: '1a9b18fc-74c0-417a-a3cc-daa03b5062a8',
              name: 'Basic',
            },
          ],
          createdOn: new Date(Date.now()),
          isLoggedIn: true,
          isDeleted: false,
          redemptionRecords: [],
          outlet: {},
        },
      ]),
    );

    service.getAllUsers().subscribe(res => {
      expect(res[0].username).toBe(users[0].username);
      expect(res[1].username).toBe(users[1].username);
    });
    http.get.calls.reset();
  });

  it('#addUser() should return the user', () => {
    const service: UserService = TestBed.get(UserService);
    const createUserObj = {
      username: 'testuser',
      password: 'testpassword',
      outletId: 'b7752156-1035-41ca-bbef3484d19affe9r',
    };

    http.post.and.returnValue(
      of({
        id: 'b7552156-1035-41ca-bbef-484d19affe94',
        username: 'testuser',
        roles: [
          {
            id: '1a9b18fc-74c0-417a-a3cc-daa03b5062a8',
            name: 'Basic',
          },
        ],
        createdOn: new Date(Date.now()),
        isLoggedIn: true,
        isDeleted: false,
        redemptionRecords: [],
        outlet: {},
      }),
    );

    service.addUser(createUserObj).subscribe(res => {
      expect(res.username).toBe('testuser');
    });
    http.post.calls.reset();
  });

  it('#deleteUser() should return the user', () => {
    const service: UserService = TestBed.get(UserService);
    const id = 'b7552156-1035-41ca-bbef-484d19affe94';

    http.delete.and.returnValue(
      of({
        id: 'b7552156-1035-41ca-bbef-484d19affe94',
        username: 'testuser',
        roles: [
          {
            id: '1a9b18fc-74c0-417a-a3cc-daa03b5062a8',
            name: 'Basic',
          },
        ],
        createdOn: new Date(Date.now()),
        isLoggedIn: true,
        isDeleted: false,
        redemptionRecords: [],
        outlet: {},
      }),
    );

    service.deleteUser(id).subscribe(res => {
      expect(res.username).toBe('testuser');
    });
    http.delete.calls.reset();
  });

  it('#updateUser() should return the user', () => {
    const service: UserService = TestBed.get(UserService);

    const user = {
      id: 'b7552156-1035-41ca-bbef-484d19affe94',
      username: 'testuser',
      roles: [
        {
          id: '1a9b18fc-74c0-417a-a3cc-daa03b5062a8',
          name: 'Basic',
        },
      ],
      createdOn: new Date(Date.now()),
      isLoggedIn: true,
      isDeleted: false,
      redemptionRecords: [],
      outlet: {},
    };

    const updateUserData = {
      username: 'testuser2',
    };

    http.put.and.returnValue(
      of({
        id: 'b7552156-1035-41ca-bbef-484d19affe94',
        username: 'testuser2',
        roles: [
          {
            id: '1a9b18fc-74c0-417a-a3cc-daa03b5062a8',
            name: 'Basic',
          },
        ],
        createdOn: new Date(Date.now()),
        isLoggedIn: true,
        isDeleted: false,
        redemptionRecords: [],
        outlet: {},
      }),
    );

    service.editUser(user.id, updateUserData).subscribe(res => {
      expect(res.username).toBe('testuser2');
    });
  });
  http.put.calls.reset();
});
