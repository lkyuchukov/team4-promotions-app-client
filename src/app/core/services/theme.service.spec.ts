import { TestBed } from '@angular/core/testing';
import { StorageService } from './storage.service';
import { ThemeService } from './theme.service';

describe('ThemeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const storage = jasmine.createSpyObj('StorageService', ['set', 'get']);
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: StorageService,
          useValue: storage,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: ThemeService = TestBed.get(ThemeService);
    expect(service).toBeTruthy();
  });
});
