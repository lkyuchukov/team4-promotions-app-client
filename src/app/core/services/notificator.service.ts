import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root',
})
export class NotificatorService {
  constructor(private snackbar: MatSnackBar) {}

  public openSnackbar(message: string) {
    this.snackbar.open(message);
  }
}
