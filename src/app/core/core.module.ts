import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AuthService } from './services/auth.service';
import { CodeService } from './services/code.service';
import { CustomerService } from './services/customer.service';
import { IssueService } from './services/issue.service';
import { LoadingScreenService } from './services/loading-screen.service';
import { NotificatorService } from './services/notificator.service';
import { OutletService } from './services/outlet.service';
import { StorageService } from './services/storage.service';
import { ThemeService } from './services/theme.service';
import { UserService } from './services/user.service';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [
    AuthService,
    NotificatorService,
    CodeService,
    IssueService,
    NotificatorService,
    OutletService,
    CustomerService,
    StorageService,
    ThemeService,
    LoadingScreenService,
    UserService,
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500 } },
  ],
})
export class CoreModule {}
