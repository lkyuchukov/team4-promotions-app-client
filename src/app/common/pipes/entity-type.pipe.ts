import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'entityType' })
export class EntityTypePipe implements PipeTransform {
  transform(value: number): string {
    if (value === 0) {
      return 'Customer';
    } else if (value === 1) {
      return 'Outlet';
    } else if (value === 2) {
      return 'User';
    } else if (value === 3) {
      return 'Code';
    } else {
      return 'Multiple Redemption Attempt';
    }
  }
}
