import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'activityAction' })
export class ActivityActionPipe implements PipeTransform {
  transform(value: number): string {
    if (value === 0) {
      return 'Created';
    } else if (value === 1) {
      return 'Updated';
    } else if (value === 2) {
      return 'Deleted';
    } else if (value === 3) {
      return 'Redeemed';
    } else {
      return 'Reported';
    }
  }
}
