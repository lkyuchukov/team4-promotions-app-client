import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'icon' })
export class IconPipe implements PipeTransform {
  transform(value: number): string {
    if (value === 0) {
      return 'add';
    } else if (value === 1) {
      return 'edit';
    } else if (value === 2) {
      return 'delete';
    } else if (value === 3) {
      return 'redeem';
    } else {
      return 'report';
    }
  }
}
