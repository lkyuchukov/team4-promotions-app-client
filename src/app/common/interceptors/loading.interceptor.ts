import {
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { LoadingScreenService } from '../../core/services/loading-screen.service';

@Injectable()
export class LoadingScreenInterceptor implements HttpInterceptor {
  constructor(private loadingScreenService: LoadingScreenService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this.loadingScreenService.startLoading();

    return next.handle(req).pipe(
      finalize(() => {
        this.loadingScreenService.stopLoading();
      }),
    );
  }
}
