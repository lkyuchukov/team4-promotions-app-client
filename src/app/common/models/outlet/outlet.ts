import { Customer } from '../customer/customer';
import { RedemptionRecord } from '../redemption-record/redemption-record';
import { User } from '../user/user';

export interface Outlet {
  id: string;
  address: string;
  city: string;
  isDeleted: boolean;
  customer: Customer;
  redemptionRecords: RedemptionRecord[];
  users: User[];
}
