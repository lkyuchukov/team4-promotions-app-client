import { Code } from '../code/code';
import { Outlet } from '../outlet/outlet';
import { User } from '../user/user';
import { Customer } from '../customer/customer';

export interface RedemptionRecord {
  id: string;
  code?: Code;
  createdOn: Date;
  isDeleted: boolean;
  outlet?: Outlet;
  redeemedOn: Date;
  user?: User;
  customer?: Customer;
}
