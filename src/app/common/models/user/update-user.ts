export interface UpdateUser {
  username: string;
  password: string;
  outletName: string;
}
