export interface UserSubject {
  username: string;
  role: string;
}
