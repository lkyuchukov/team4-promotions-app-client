export interface CreateUser {
  username: string;
  password: string;
  outletId: string;
}
