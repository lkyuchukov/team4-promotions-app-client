import { Outlet } from '../outlet/outlet';

export interface User {
  id: string;
  username: string;
  roles: [{ id: string; name: string }];
  createdOn: Date;
  updatedOn: Date;
  isDeleted: boolean;
  isLoggedIn: boolean;
  password?: string;
  outlet?: Outlet;
}
