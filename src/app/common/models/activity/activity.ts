import { User } from '../user/user';

export interface Activity {
  id: string;
  action: string;
  entityType: string;
  user: User;
  createdOn: Date;
}
