import { Outlet } from '../outlet/outlet';

export interface Customer {
  id: string;
  name: string;
  isDeleted: boolean;
  outlets: Outlet[];
}
