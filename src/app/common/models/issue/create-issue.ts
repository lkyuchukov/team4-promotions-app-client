export interface CreateIssue {
  title: string;
  description: string;
}
