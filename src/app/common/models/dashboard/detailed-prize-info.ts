export interface DetailedPrizeInfo {
  'Small prize': number;
  'Medium prize': number;
  'Big prize': number;
}
