export interface PrizeCount {
  smallPrizesCount: number;
  mediumPrizesCount: number;
  bigPrizesCount: number;
}
