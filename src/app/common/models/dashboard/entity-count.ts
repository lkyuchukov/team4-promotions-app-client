export interface EntityCount {
  customerCount: number;
  outletCount: number;
  userCount: number;
}
