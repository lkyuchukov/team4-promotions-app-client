import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../common/guards/auth.guard';
import { RolesGuard } from '../common/guards/roles.guard';
import { OutletResolverService } from '../outlets/outlet-resolver.service';
import { UserDetailResolverService } from './user-detail/user-detail-resolver.service';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserResolverService } from './user-resolver.service';
import { UserComponent } from './user.component';

const userRoutes: Routes = [
  {
    path: '',
    component: UserComponent,
    canActivate: [AuthGuard, RolesGuard],
    resolve: {
      users: UserResolverService,
    },
  },
  {
    path: ':id',
    component: UserDetailComponent,
    canActivate: [AuthGuard, RolesGuard],
    resolve: {
      user: UserDetailResolverService,
      outlets: OutletResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
