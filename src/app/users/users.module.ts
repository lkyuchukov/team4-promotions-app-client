import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ChangeOutletDialogComponent } from './user-detail/change-outlet-dialog/change-outlet-dialog.component';
import { ChangeOutletComponent } from './user-detail/change-outlet/change-outlet.component';
import { DeleteUserDialogComponent } from './user-detail/delete-user-dialog/delete-user-dialog.component';
import { DeleteUserComponent } from './user-detail/delete-user/delete-user.component';
import { EditUserDialogComponent } from './user-detail/edit-user-dialog/edit-user-dialog.component';
import { EditUserComponent } from './user-detail/edit-user/edit-user.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserTableComponent } from './user-table/user-table.component';
import { UserComponent } from './user.component';
import { UsersRoutingModule } from './users-routing.module';

@NgModule({
  entryComponents: [
    DeleteUserDialogComponent,
    EditUserDialogComponent,
    ChangeOutletDialogComponent,
  ],
  declarations: [
    UserDetailComponent,
    EditUserComponent,
    DeleteUserComponent,
    DeleteUserDialogComponent,
    ChangeOutletComponent,
    UserTableComponent,
    UserComponent,
    EditUserDialogComponent,
    ChangeOutletDialogComponent,
  ],
  imports: [SharedModule, UsersRoutingModule],
})
export class UsersModule {}
