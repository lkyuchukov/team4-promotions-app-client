import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { SharedModule } from '../shared/shared.module';
import { UserTableComponent } from './user-table/user-table.component';
import { UserComponent } from './user.component';

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule, BrowserAnimationsModule],
      declarations: [UserComponent, UserTableComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              users: [
                {
                  createdOn: new Date(Date.now()),
                  id: '2ea3afce-fd99-4879-9298-8cebb8710836',
                  isDeleted: false,
                  isLoggedIn: false,
                  username: 'test',
                  outlet: [],
                  roles: [],
                  updatedOn: new Date(Date.now()),
                },
              ],
            }),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('users should be defined', () => {
    expect(component.users).toBeTruthy();
  });
});
