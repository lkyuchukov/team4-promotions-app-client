import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { User } from '../common/models/user/user';
import { NotificatorService } from '../core/services/notificator.service';
import { UserService } from '../core/services/user.service';

@Injectable({
  providedIn: 'root',
})
export class UserResolverService implements Resolve<User[]> {
  constructor(
    private userService: UserService,
    private notificator: NotificatorService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.userService.getAllUsers().pipe(
      mergeMap(users => {
        if (users) {
          return of(users);
        }
      }),
      catchError(() => {
        this.notificator.openSnackbar('Sorry, something went wrong');
        return of([{}]);
      }),
    );
  }
}
