import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { SharedModule } from '../../shared/shared.module';
import { ChangeOutletComponent } from './change-outlet/change-outlet.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserDetailComponent } from './user-detail.component';

describe('UserDetailComponent', () => {
  let component: UserDetailComponent;
  let fixture: ComponentFixture<UserDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule, BrowserAnimationsModule],
      declarations: [
        UserDetailComponent,
        EditUserComponent,
        ChangeOutletComponent,
        DeleteUserComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              user: {
                createdOn: new Date(Date.now()),
                id: '2ea3afce-fd99-4879-9298-8cebb8710836',
                isDeleted: false,
                isLoggedIn: false,
                username: 'test',
                outlet: [],
                roles: [],
                updatedOn: new Date(Date.now()),
              },
            }),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('userInfo should be defined', () => {
    expect(component.userInfo).toBeTruthy();
  });
});
