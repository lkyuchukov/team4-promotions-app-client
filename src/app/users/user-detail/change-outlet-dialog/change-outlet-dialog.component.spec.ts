import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserService } from '../../../core/services/user.service';
import { SharedModule } from '../../../shared/shared.module';
import { ChangeOutletDialogComponent } from './change-outlet-dialog.component';

describe('ChangeOutletDialogComponent', () => {
  let component: ChangeOutletDialogComponent;
  let fixture: ComponentFixture<ChangeOutletDialogComponent>;

  beforeEach(async(() => {
    const userServiceSpy = jasmine.createSpyObj('UserService', ['editUser']);
    TestBed.configureTestingModule({
      imports: [SharedModule, BrowserAnimationsModule],
      declarations: [ChangeOutletDialogComponent],
      providers: [
        {
          provide: UserService,
          useValue: userServiceSpy,
        },
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeOutletDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
