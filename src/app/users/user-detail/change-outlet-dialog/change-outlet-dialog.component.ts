import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { User } from '../../../common/models/user/user';
import { NotificatorService } from '../../../core/services/notificator.service';
import { UserService } from '../../../core/services/user.service';

@Component({
  selector: 'app-change-outlet-dialog',
  templateUrl: './change-outlet-dialog.component.html',
  styleUrls: ['./change-outlet-dialog.component.css'],
})
export class ChangeOutletDialogComponent implements OnInit {
  outletControl = new FormControl('', [Validators.required]);

  constructor(
    private userService: UserService,
    private notificator: NotificatorService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit() {}

  changeOutlet() {
    const newOutletName = this.outletControl.value;
    this.userService
      .editUser(this.data.userInfo.id, { outletName: newOutletName })
      .subscribe(
        (res: User) => {
          this.data.userInfo.outlet = res.outlet;
          this.notificator.openSnackbar('Successfully changed outlet.');
        },
        () => this.notificator.openSnackbar('Something went wrong.'),
      );
  }
}
