import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { User } from '../../../common/models/user/user';
import { DeleteUserDialogComponent } from '../delete-user-dialog/delete-user-dialog.component';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css'],
})
export class DeleteUserComponent implements OnInit {
  @Input()
  userInfo: User;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  openDialog() {
    // apply css styles to dialog here
    const dialogRef = this.dialog.open(DeleteUserDialogComponent, {
      data: this.userInfo.id,
    });
  }
}
