import { Location } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NotificatorService } from '../../../core/services/notificator.service';
import { UserService } from '../../../core/services/user.service';

@Component({
  selector: 'app-delete-user-dialog',
  templateUrl: './delete-user-dialog.component.html',
  styleUrls: ['./delete-user-dialog.component.css'],
})
export class DeleteUserDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DeleteUserDialogComponent>,
    private userService: UserService,
    private notificator: NotificatorService,
    private location: Location,
    @Inject(MAT_DIALOG_DATA) public data: string,
  ) {}

  ngOnInit() {}

  deleteUser() {
    this.userService.deleteUser(this.data).subscribe(
      () => {
        this.location.back();
        this.notificator.openSnackbar('User deleted.');
      },
      () => {
        this.notificator.openSnackbar('Could not delete user.');
      },
    );
  }
}
