import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { of } from 'rxjs';
import { catchError, mergeMap, take } from 'rxjs/operators';
import { User } from '../../common/models/user/user';
import { NotificatorService } from '../../core/services/notificator.service';
import { UserService } from '../../core/services/user.service';

@Injectable({
  providedIn: 'root',
})
export class UserDetailResolverService implements Resolve<User> {
  constructor(
    private userService: UserService,
    private notificator: NotificatorService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const id = route.paramMap.get('id');

    return this.userService.getUser(id).pipe(
      // this ensures the observable completes after retrieving the first value
      take(1),
      mergeMap(user => {
        if (user) {
          return of(user);
        }
      }),
      catchError(() => {
        this.notificator.openSnackbar('Failed to get user data.');
        return of({});
      }),
    );
  }
}
