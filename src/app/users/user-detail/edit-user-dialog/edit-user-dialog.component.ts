import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { User } from '../../../common/models/user/user';
import { NotificatorService } from '../../../core/services/notificator.service';
import { UserService } from '../../../core/services/user.service';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.css'],
})
export class EditUserDialogComponent implements OnInit {
  editUserForm: FormGroup;

  constructor(
    private userService: UserService,
    private notificator: NotificatorService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: User,
  ) {}

  ngOnInit() {
    this.editUserForm = this.formBuilder.group({
      username: [`${this.data.username}`, [Validators.required]],
    });
  }

  editUser() {
    const userData = this.editUserForm.value;
    this.userService.editUser(this.data.id, userData).subscribe(
      () => {
        this.data.username = userData.username;
        this.notificator.openSnackbar('User data edited.');
      },
      () => {
        this.notificator.openSnackbar('Sorry, something went wrong.');
      },
    );
  }
}
