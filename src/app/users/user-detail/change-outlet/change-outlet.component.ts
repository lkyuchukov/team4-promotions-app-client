import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Outlet } from '../../../common/models/outlet/outlet';
import { User } from '../../../common/models/user/user';
import { ChangeOutletDialogComponent } from '../change-outlet-dialog/change-outlet-dialog.component';

@Component({
  selector: 'app-change-outlet',
  templateUrl: './change-outlet.component.html',
  styleUrls: ['./change-outlet.component.css'],
})
export class ChangeOutletComponent implements OnInit {
  @Input()
  userInfo: User;

  @Input()
  outlets: Outlet[];

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  openDialog() {
    const dialogRef = this.dialog.open(ChangeOutletDialogComponent, {
      data: { userInfo: this.userInfo, outlets: this.outlets },
      width: '450px',
    });
  }
}
