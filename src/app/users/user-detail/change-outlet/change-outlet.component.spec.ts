import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeOutletComponent } from './change-outlet.component';
import { SharedModule } from '../../../shared/shared.module';

describe('ChangeOutletComponent', () => {
  let component: ChangeOutletComponent;
  let fixture: ComponentFixture<ChangeOutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ChangeOutletComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
