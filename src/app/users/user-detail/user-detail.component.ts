import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Outlet } from '../../common/models/outlet/outlet';
import { User } from '../../common/models/user/user';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
})
export class UserDetailComponent implements OnInit {
  userInfo: User;

  outlets: Outlet[];

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.subscribe((data: { user: User; outlets: Outlet[] }) => {
      this.userInfo = data.user;
      this.outlets = data.outlets;
    });
  }
}
