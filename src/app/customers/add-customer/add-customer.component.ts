import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Customer } from '../../common/models/customer/customer';
import { AddCustomerDialogComponent } from '../add-customer-dialog/add-customer-dialog/add-customer-dialog.component';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css'],
})
export class AddCustomerComponent implements OnInit {
  @Input() customers: Customer[];

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  openDialog() {
    const dialogRef = this.dialog.open(AddCustomerDialogComponent, {
      width: '450px',
      data: this.customers,
    });
  }
}
