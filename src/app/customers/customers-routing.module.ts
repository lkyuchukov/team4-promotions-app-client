import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolesGuard } from '../common/guards/roles.guard';
import { CustomerDetailResolverService } from './customer-detail/customer-detail-resolver.service';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CustomerResolverService } from './customer-resolver.service';
import { CustomerComponent } from './customer.component';
import { AuthGuard } from '../common/guards/auth.guard';

const customerRoutes: Routes = [
  {
    path: '',
    component: CustomerComponent,
    canActivate: [AuthGuard, RolesGuard],
    resolve: {
      customers: CustomerResolverService,
    },
  },
  {
    path: ':id',
    component: CustomerDetailComponent,
    canActivate: [AuthGuard, RolesGuard],
    resolve: {
      customer: CustomerDetailResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(customerRoutes)],
  exports: [RouterModule],
})
export class CustomersRoutingModule {}
