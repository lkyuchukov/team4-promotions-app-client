import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { CustomerService } from '../core/services/customer.service';
import { NotificatorService } from '../core/services/notificator.service';
import { Customer } from '../common/models/customer/customer';

@Injectable({
  providedIn: 'root',
})
export class CustomerResolverService implements Resolve<Customer[]> {
  constructor(
    private customerService: CustomerService,
    private notificator: NotificatorService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.customerService.getAllCustomers().pipe(
      mergeMap(customers => {
        if (customers) {
          return of(customers);
        }
      }),
      catchError(() => {
        this.notificator.openSnackbar('Sorry, something went wrong');
        return of([{}]);
      }),
    );
  }
}
