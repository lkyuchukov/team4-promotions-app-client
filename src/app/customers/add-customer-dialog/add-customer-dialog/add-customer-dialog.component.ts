import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Customer } from '../../../common/models/customer/customer';
import { CustomerService } from '../../../core/services/customer.service';
import { NotificatorService } from '../../../core/services/notificator.service';

@Component({
  selector: 'app-add-customer-dialog',
  templateUrl: './add-customer-dialog.component.html',
  styleUrls: ['./add-customer-dialog.component.css'],
})
export class AddCustomerDialogComponent implements OnInit {
  addCustomerForm: FormGroup;

  constructor(
    private customerService: CustomerService,
    private formBuilder: FormBuilder,
    private notificator: NotificatorService,
    public dialogRef: MatDialogRef<AddCustomerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Customer[],
  ) {}

  ngOnInit() {
    this.addCustomerForm = this.formBuilder.group({
      name: ['', [Validators.required]],
    });
  }

  addCustomer() {
    const customerData = this.addCustomerForm.value;
    this.dialogRef.close();
    this.customerService.addCustomer(customerData).subscribe(
      res => {
        this.data.push(res);
        this.notificator.openSnackbar('Customer added.');
      },
      err => {
        this.notificator.openSnackbar(err.error.message);
      },
    );
  }
}
