import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { CustomerService } from '../../../core/services/customer.service';
import { NotificatorService } from '../../../core/services/notificator.service';
import { SharedModule } from '../../../shared/shared.module';
import { EditCustomerDialogComponent } from './edit-customer-dialog.component';

describe('EditCustomerDialogComponent', () => {
  let component: EditCustomerDialogComponent;
  let fixture: ComponentFixture<EditCustomerDialogComponent>;
  const formBuilder = new FormBuilder();

  beforeEach(async(() => {
    const notificatorSpy = jasmine.createSpyObj('NotificatorService', [
      'openSnackbar',
    ]);
    const customerServiceSpy = jasmine.createSpyObj('CustomerService', [
      'editCustomer',
    ]);

    TestBed.configureTestingModule({
      imports: [SharedModule, BrowserAnimationsModule, RouterTestingModule],
      declarations: [EditCustomerDialogComponent],
      providers: [
        { provide: FormBuilder, useValue: formBuilder },
        {
          provide: CustomerService,
          useValue: customerServiceSpy,
        },
        {
          provide: NotificatorService,
          useValue: notificatorSpy,
        },
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCustomerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
