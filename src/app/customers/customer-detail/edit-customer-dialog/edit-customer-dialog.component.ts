import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Customer } from '../../../common/models/customer/customer';
import { CustomerService } from '../../../core/services/customer.service';
import { NotificatorService } from '../../../core/services/notificator.service';

@Component({
  selector: 'app-edit-customer-dialog',
  templateUrl: './edit-customer-dialog.component.html',
  styleUrls: ['./edit-customer-dialog.component.css'],
})
export class EditCustomerDialogComponent implements OnInit {
  editCustomerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private notificator: NotificatorService,
    private customerService: CustomerService,
    public dialogRef: MatDialogRef<EditCustomerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Customer,
  ) {}

  ngOnInit() {
    this.editCustomerForm = this.formBuilder.group({
      name: [`${this.data.name}`, [Validators.required]],
    });
  }

  editCustomer() {
    const customerData = this.editCustomerForm.value;
    this.dialogRef.close();
    this.customerService.editCustomer(this.data.id, customerData).subscribe(
      res => {
        this.data.name = customerData.name;
        this.notificator.openSnackbar('Customer data edited.');
      },
      () => {
        this.notificator.openSnackbar('Sorry, something went wrong.');
      },
    );
  }
}
