import { Location } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { SharedModule } from '../../shared/shared.module';
import { AddOutletComponent } from './add-outlet/add-outlet.component';
import { CustomerDetailComponent } from './customer-detail.component';
import { DeleteCustomerComponent } from './delete-customer/delete-customer.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { OutletListComponent } from './outlet-list/outlet-list.component';

describe('CustomerDetailComponent', () => {
  let component: CustomerDetailComponent;
  let fixture: ComponentFixture<CustomerDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule, BrowserAnimationsModule],
      declarations: [
        CustomerDetailComponent,
        AddOutletComponent,
        EditCustomerComponent,
        DeleteCustomerComponent,
        OutletListComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              customer: {
                createdOn: new Date(Date.now()),
                id: '2ea3afce-fd99-4879-9298-8cebb8710836',
                isDeleted: false,
                name: 'test',
                outlets: [],
                updatedOn: new Date(Date.now()),
              },
            }),
          },
        },
        { provide: Location, useValue: {} },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('customerInfo should be defined', () => {
    expect(component.customerInfo).toBeTruthy();
  });
});
