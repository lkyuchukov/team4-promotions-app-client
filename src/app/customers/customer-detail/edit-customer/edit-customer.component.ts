import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Customer } from '../../../common/models/customer/customer';
import { EditCustomerDialogComponent } from '../edit-customer-dialog/edit-customer-dialog.component';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css'],
})
export class EditCustomerComponent implements OnInit {
  @Input()
  customerInfo: Customer;

  constructor(public dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(EditCustomerDialogComponent, {
      data: this.customerInfo,
      width: '450px',
    });
  }

  ngOnInit() {}
}
