import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { of } from 'rxjs';
import { catchError, mergeMap, take } from 'rxjs/operators';
import { Customer } from '../../common/models/customer/customer';
import { CustomerService } from '../../core/services/customer.service';
import { NotificatorService } from '../../core/services/notificator.service';

@Injectable({
  providedIn: 'root',
})
export class CustomerDetailResolverService implements Resolve<Customer> {
  constructor(
    private customerService: CustomerService,
    private notificator: NotificatorService,
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const id = route.paramMap.get('id');

    return this.customerService.getCustomer(id).pipe(
      // this ensures the observable completes after retrieving the first value
      take(1),
      mergeMap(customer => {
        if (customer) {
          return of(customer);
        }
      }),
      catchError(() => {
        this.notificator.openSnackbar('Could not get customer data.');
        return of({});
      }),
    );
  }
}
