import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { CustomerService } from '../../../core/services/customer.service';
import { NotificatorService } from '../../../core/services/notificator.service';
import { SharedModule } from '../../../shared/shared.module';
import { DeleteCustomerDialogComponent } from './delete-customer-dialog.component';

describe('DeleteCustomerDialogComponent', () => {
  let component: DeleteCustomerDialogComponent;
  let fixture: ComponentFixture<DeleteCustomerDialogComponent>;
  const formBuilder = new FormBuilder();

  beforeEach(async(() => {
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    const notificatorSpy = jasmine.createSpyObj('NotificatorService', [
      'openSnackbar',
    ]);
    const customerServiceSpy = jasmine.createSpyObj('CustomerService', [
      'deleteCustomer',
    ]);

    TestBed.configureTestingModule({
      imports: [SharedModule, BrowserAnimationsModule],
      declarations: [DeleteCustomerDialogComponent],
      providers: [
        { provide: FormBuilder, useValue: formBuilder },
        {
          provide: CustomerService,
          useValue: customerServiceSpy,
        },
        {
          provide: NotificatorService,
          useValue: notificatorSpy,
        },
        {
          provide: Router,
          useValue: routerSpy,
        },
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCustomerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
