import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { CustomerService } from '../../../core/services/customer.service';
import { NotificatorService } from '../../../core/services/notificator.service';

@Component({
  selector: 'app-delete-customer-dialog',
  templateUrl: './delete-customer-dialog.component.html',
  styleUrls: ['./delete-customer-dialog.component.css'],
})
export class DeleteCustomerDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DeleteCustomerDialogComponent>,
    private customerService: CustomerService,
    private notificator: NotificatorService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: string,
  ) {}

  ngOnInit() {}

  deleteCustomer() {
    this.customerService.deleteCustomer(this.data).subscribe(
      () => {
        this.router.navigate(['/customers']);
        this.notificator.openSnackbar('Customer deleted.');
      },
      () => {
        this.notificator.openSnackbar('Sorry, something went wrong.');
      },
    );
  }
}
