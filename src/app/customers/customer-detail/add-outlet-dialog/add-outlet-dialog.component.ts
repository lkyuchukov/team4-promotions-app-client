import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Customer } from '../../../common/models/customer/customer';
import { NotificatorService } from '../../../core/services/notificator.service';
import { OutletService } from '../../../core/services/outlet.service';

@Component({
  selector: 'app-add-outlet-dialog',
  templateUrl: './add-outlet-dialog.component.html',
  styleUrls: ['./add-outlet-dialog.component.css'],
})
export class AddOutletDialogComponent implements OnInit {
  addOutletForm: FormGroup;

  constructor(
    private outletService: OutletService,
    private formBuilder: FormBuilder,
    private notificator: NotificatorService,
    public dialogRef: MatDialogRef<AddOutletDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Customer,
  ) {}

  ngOnInit() {
    this.addOutletForm = this.formBuilder.group({
      address: ['', [Validators.required]],
      city: ['', [Validators.required]],
    });
  }

  addOutlet() {
    const outletData = this.addOutletForm.value;
    outletData.customer = this.data.name;
    this.outletService.addOutlet(outletData).subscribe(
      res => {
        this.data.outlets.push(res);
        this.notificator.openSnackbar('Outlet added.');
      },
      err => {
        this.notificator.openSnackbar(err.error.message);
      },
    );
  }
}
