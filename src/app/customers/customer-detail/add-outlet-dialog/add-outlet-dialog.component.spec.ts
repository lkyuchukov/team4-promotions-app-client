import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { NotificatorService } from '../../../core/services/notificator.service';
import { OutletService } from '../../../core/services/outlet.service';
import { SharedModule } from '../../../shared/shared.module';
import { AddOutletDialogComponent } from './add-outlet-dialog.component';

describe('AddOutletDialogComponent', () => {
  let component: AddOutletDialogComponent;
  let fixture: ComponentFixture<AddOutletDialogComponent>;
  const formBuilder = new FormBuilder();

  beforeEach(async(() => {
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    const notificatorSpy = jasmine.createSpyObj('NotificatorService', [
      'openSnackbar',
    ]);
    const outletServiceSpy = jasmine.createSpyObj('OutletService', [
      'addOutlet',
    ]);

    TestBed.configureTestingModule({
      imports: [SharedModule, BrowserAnimationsModule],
      declarations: [AddOutletDialogComponent],
      providers: [
        { provide: FormBuilder, useValue: formBuilder },
        {
          provide: OutletService,
          useValue: outletServiceSpy,
        },
        {
          provide: NotificatorService,
          useValue: notificatorSpy,
        },
        {
          provide: Router,
          useValue: routerSpy,
        },
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOutletDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
