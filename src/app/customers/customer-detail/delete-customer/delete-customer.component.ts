import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Customer } from '../../../common/models/customer/customer';
import { DeleteCustomerDialogComponent } from '../delete-customer-dialog/delete-customer-dialog.component';

@Component({
  selector: 'app-delete-customer',
  templateUrl: './delete-customer.component.html',
  styleUrls: ['./delete-customer.component.css'],
})
export class DeleteCustomerComponent {
  @Input()
  customerInfo: Customer;

  constructor(public dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(DeleteCustomerDialogComponent, {
      data: this.customerInfo.id,
    });
  }
}
