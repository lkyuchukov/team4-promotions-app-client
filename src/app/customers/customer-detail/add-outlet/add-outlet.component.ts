import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Customer } from '../../../common/models/customer/customer';
import { AddOutletDialogComponent } from '../add-outlet-dialog/add-outlet-dialog.component';

@Component({
  selector: 'app-add-outlet',
  templateUrl: './add-outlet.component.html',
  styleUrls: ['./add-outlet.component.css'],
})
export class AddOutletComponent implements OnInit {
  @Input()
  customerInfo: Customer;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  openDialog() {
    const dialogRef = this.dialog.open(AddOutletDialogComponent, {
      width: '450px',
      data: this.customerInfo,
    });
  }
}
