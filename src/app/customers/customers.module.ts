import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AddCustomerDialogComponent } from './add-customer-dialog/add-customer-dialog/add-customer-dialog.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { AddOutletDialogComponent } from './customer-detail/add-outlet-dialog/add-outlet-dialog.component';
import { AddOutletComponent } from './customer-detail/add-outlet/add-outlet.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { DeleteCustomerDialogComponent } from './customer-detail/delete-customer-dialog/delete-customer-dialog.component';
import { DeleteCustomerComponent } from './customer-detail/delete-customer/delete-customer.component';
import { EditCustomerDialogComponent } from './customer-detail/edit-customer-dialog/edit-customer-dialog.component';
import { EditCustomerComponent } from './customer-detail/edit-customer/edit-customer.component';
import { OutletListComponent } from './customer-detail/outlet-list/outlet-list.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerComponent } from './customer.component';
import { CustomersRoutingModule } from './customers-routing.module';

@NgModule({
  entryComponents: [
    DeleteCustomerDialogComponent,
    AddCustomerDialogComponent,
    EditCustomerDialogComponent,
    AddOutletDialogComponent,
  ],
  declarations: [
    CustomerComponent,
    CustomerListComponent,
    CustomerDetailComponent,
    AddCustomerComponent,
    DeleteCustomerComponent,
    EditCustomerComponent,
    DeleteCustomerDialogComponent,
    OutletListComponent,
    AddCustomerDialogComponent,
    EditCustomerDialogComponent,
    AddOutletComponent,
    AddOutletDialogComponent,
  ],
  imports: [SharedModule, CustomersRoutingModule],
})
export class CustomersModule {}
