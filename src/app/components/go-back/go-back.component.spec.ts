import { Location } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '../../shared/shared.module';
import { GoBackComponent } from './go-back.component';

describe('GoBackComponent', () => {
  let component: GoBackComponent;
  let fixture: ComponentFixture<GoBackComponent>;

  beforeEach(async(() => {
    const locationSpy = jasmine.createSpyObj('Location', ['back']);
    TestBed.configureTestingModule({
      imports: [SharedModule],
      providers: [
        {
          provide: Location,
          useValue: locationSpy,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should call #location.back()', async(() => {
    spyOn(component, 'goBack');
    const goBackButton = fixture.debugElement.nativeElement.querySelector(
      'button',
    );
    goBackButton.click();

    fixture.whenStable().then(() => {
      expect(component.goBack).toHaveBeenCalled();
    });
  }));
});
