import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoadingScreenService } from '../../core/services/loading-screen.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  @Input()
  loggedIn: boolean;

  @Input()
  username: string;

  @Input()
  role: string;

  @Output()
  logout = new EventEmitter<undefined>();

  opened = true;

  isLoading: boolean;

  loadingSubscription: Subscription;

  constructor(
    private loadingScreenService: LoadingScreenService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.loadingSubscription = this.loadingScreenService
      .getLoadingStatus()
      .subscribe(status => (this.isLoading = status));
  }

  triggerLogout() {
    this.logout.emit();
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }

  goToHelpdesk() {
    this.router.navigate(['helpdesk']);
  }
}
