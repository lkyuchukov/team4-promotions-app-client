import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { LoadingScreenService } from '../../core/services/loading-screen.service';
import { SharedModule } from '../../shared/shared.module';
import { LoadingScreenComponent } from '../loading-screen/loading-screen.component';
import { AdminComponent } from './admin/admin.component';
import { NavbarComponent } from './navbar.component';
import { ThemePickerComponent } from './theme-picker/theme-picker.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  const loadingServiceStub = {
    getLoadingStatus: () => of(false),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
      ],
      declarations: [
        NavbarComponent,
        ThemePickerComponent,
        AdminComponent,
        LoadingScreenComponent,
      ],
      providers: [
        { provide: LoadingScreenService, useValue: loadingServiceStub },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should call #loadingService.getLoadingStatus()', () => {
    fixture.detectChanges();
    expect(component.loadingSubscription).toBeTruthy();
  });
});
