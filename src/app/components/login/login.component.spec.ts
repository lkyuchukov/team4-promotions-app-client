import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { UserLogin } from '../../common/models/user/login-user';
import { AuthService } from '../../core/services/auth.service';
import { NotificatorService } from '../../core/services/notificator.service';
import { SharedModule } from '../../shared/shared.module';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  const formBuilder = new FormBuilder();

  const authServiceStub = {
    login: (user: UserLogin) =>
      of({
        token: 'token',
        user: {
          username: 'testuser1',
          id: '2e679030-9d12-473d-b6e6-902116093926',
          roles: [
            {
              id: '54957d43-a4ed-40d8-8e89-c93665eff516',
              name: 'Basic',
            },
          ],
          createdOn: new Date(Date.now()),
          isLoggedIn: true,
          isBanned: false,
        },
      }),
  };

  beforeEach(async(() => {
    const notificatorSpy = jasmine.createSpyObj('NotificatorService', [
      'openSnackbar',
    ]);

    TestBed.configureTestingModule({
      imports: [SharedModule, BrowserAnimationsModule, RouterTestingModule],
      declarations: [LoginComponent],
      providers: [
        { provide: FormBuilder, useValue: formBuilder },
        {
          provide: AuthService,
          useValue: authServiceStub,
        },
        {
          provide: NotificatorService,
          useValue: notificatorSpy,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  it('should be created', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('#login should call #authService.login() with the correct user data', () => {
    const service = fixture.debugElement.injector.get(AuthService);
    spyOn(service, 'login').and.callThrough();
    component.loginForm = formBuilder.group({
      username: 'testuser',
      password: 'password',
    });

    component.login();

    fixture.whenStable().then(() => {
      expect(service.login).toHaveBeenCalledWith(component.loginForm.value);
    });
  });

  it('should contain a <form> within a <div>', () => {
    const loginDe = fixture.debugElement;
    const divDe = loginDe.query(By.css('div'));
    const div: HTMLElement = divDe.nativeElement;

    expect(div.querySelector('form')).not.toBe(null);
  });

  it('submit button should be disabled when form is empty', () => {
    component.loginForm = formBuilder.group({
      username: '',
      password: '',
    });

    fixture.detectChanges();

    const submitEl: DebugElement = fixture.debugElement.query(By.css('button'));

    expect(submitEl.nativeElement.disabled).toBe(true);
  });
});
