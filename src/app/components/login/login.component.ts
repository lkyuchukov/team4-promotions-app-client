import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserLogin } from '../../common/models/user/login-user';
import { AuthService } from '../../core/services/auth.service';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private notificator: NotificatorService,
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(5)]],
    });
  }

  login() {
    const user: UserLogin = this.loginForm.value;
    this.authService.login(user).subscribe(
      data => {
        if (data.user.roles[0].name === 'Basic') {
          this.router.navigate(['/codes']);
        } else if (data.user.roles[0].name === 'Admin') {
          this.router.navigate(['/dashboard']);
        }
      },
      () => {
        this.notificator.openSnackbar('Login failed.');
      },
    );
  }
}
