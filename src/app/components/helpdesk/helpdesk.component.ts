import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IssueService } from '../../core/services/issue.service';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-helpdesk',
  templateUrl: './helpdesk.component.html',
  styleUrls: ['./helpdesk.component.css'],
})
export class HelpdeskComponent implements OnInit {
  submitIssueForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private issueService: IssueService,
    private notificator: NotificatorService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.submitIssueForm = this.formBuilder.group({
      title: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(35),
        ],
      ],
      description: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
        ],
      ],
    });
  }

  submitIssue() {
    const issue = this.submitIssueForm.value;
    this.issueService.submitIssue(issue).subscribe(
      () => {
        this.notificator.openSnackbar('Issue submitted.');
      },
      () => {
        this.notificator.openSnackbar('Something went wrong.');
      },
    );
  }
}
