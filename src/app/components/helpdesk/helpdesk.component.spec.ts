import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { IssueService } from '../../core/services/issue.service';
import { NotificatorService } from '../../core/services/notificator.service';
import { SharedModule } from '../../shared/shared.module';
import { HelpdeskComponent } from './helpdesk.component';

describe('HelpdeskComponent', () => {
  let component: HelpdeskComponent;
  let fixture: ComponentFixture<HelpdeskComponent>;
  const formBuilder = new FormBuilder();

  const issueServiceStub = {
    submitIssue: (issue: any) => of({}),
  };

  beforeEach(async(() => {
    const notificatorSpy = jasmine.createSpyObj('NotificatorService', [
      'openSnackbar',
    ]);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, SharedModule, BrowserAnimationsModule],
      declarations: [HelpdeskComponent],
      providers: [
        { provide: NotificatorService, useValue: notificatorSpy },
        { provide: IssueService, useValue: issueServiceStub },
        { provide: FormBuilder, useValue: formBuilder },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpdeskComponent);
    component = fixture.componentInstance;
  });

  it('should be created', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('#submitIssue should call #issueService.submitIssue() with the correct data', () => {
    const service = fixture.debugElement.injector.get(IssueService);
    spyOn(service, 'submitIssue').and.callThrough();
    component.submitIssueForm = formBuilder.group({
      title: 'test title',
      description: 'test description',
    });

    component.submitIssue();

    fixture.whenStable().then(() => {
      expect(service.submitIssue).toHaveBeenCalledWith(
        component.submitIssueForm.value,
      );
    });
  });
});
