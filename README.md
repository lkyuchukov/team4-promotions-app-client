# Promotions App/CRM for Coca Cola

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9. <br />
To run this project locally, you must first setup your Node.js server. <br /> 
Go to https://gitlab.com/lkyuchukov/team4-promotions-app for further instructions. 
The project should be accessible on http://d1untg8ccd0t35.cloudfront.net/ <br />
See below for credentials for login. <br />
If the link doesn't work, that means my AWS free tier credit has run out.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Login

Use the following credentials to login as the administrator:
username: admin <br />
password: password <br />

## Submit issue/multiple redemption attempt
Issues and multiple redemption attempts are submitted to https://ethereal.email/. 

Login with the following credentials to view issues: <br />
email: judge.becker@ethereal.email <br />
password: VVDK8sW3upgMJ5hyam <br />

Keep in mind that this is a fake email service and messages will not be saved.

